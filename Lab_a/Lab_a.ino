#include <Servo.h>

int izq = 1;        //Inicializa como HIGH
int der = 1;        //Inicializa como HIGH
Servo miServo;                    // Crea un objeto de tipo servomotor llamado miServo

void setup() {
  //Entrada digital para girar a la derecha (Configurado por defecto como HIGH)
  pinMode(5, INPUT_PULLUP);
  //Entrada digital para girar a la izquierda (Configurado por defecto como HIGH)
  pinMode(3, INPUT_PULLUP);
  miServo.attach(2);              // Señal del servomotor al pin digital 2
  Serial.begin(9600);
}

void loop() {
  izq = digitalRead(5);           //Lectura de pulsador de izquierda
  der = digitalRead(3);           //Lectura de pulsador de derecha
  if (izq == LOW && der == LOW) {
    miServo.write(90);           // Mueve el servomotor a la posición de 90 grados
    Serial.println("90 grados"); //Imprime Ángulo
  }
  else if (der == LOW ) {
    miServo.write(5);             // Mueve el servomotor a la posición de 5 grados
    Serial.println("0 grados");   //Imprime Ángulo
  }
  else if (izq == LOW) {
    miServo.write(180);           // Mueve el servomotor a la posición de 180 grados
    Serial.println("180 grados"); //Imprime Ángulo
  }

  delay(50);                      // Espera 50 milisegundos
}
