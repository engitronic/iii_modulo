
#include <Servo.h>   // Incluimos la libreria para servomotores
Servo miServo;    // Creando objeto de la libreria "Servo.h"

int joystickX = 5;  // Pin analogo del eje X
double angulo;      // Variable que almacena la posicion ANGULAR del joystick
void setup()
{
  miServo.attach(13);  // Pin del PWM usado para el servomotor
  Serial.begin(9600);  // Inicia la comunicacion serial a 9600 baudios
}

void loop()
{
  angulo = map(analogRead(joystickX), 0, 1023, 0, 180);  // Escala al ejeX de 10 a -10

  //Imprimiendo en tiempo real  
  Serial.print("\n\n");
  Serial.print("angulo: ");
  Serial.println(angulo);

  //El servo seguira el angulo de joystick
  miServo.write(angulo);
  
  // Espera a que el servomotor llegue a la posicion indicada
  delay(15);    
}

